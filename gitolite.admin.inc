<?php
/**
 * @file
 * Configures a form to set up Gitolite's connection and main configuration.
 */

/**
 * Configures basic settings to connect and manage Gitolite
 */
function gitolite_admin_form() {
  $form = array();
  $options = node_get_types('names');
  $form['gitolite_content_type_name'] = array(
    '#type' => 'radios',
    '#title' => t('Select a content type to add the field "Git repositories" to'),
    '#options' => $options,
    '#default_value' => variable_get('gitolite_content_type_name', ''),
    '#description' => t('The selected content type must be previously set as "node group" at !link.',
                        array('!link' => l(t('Organic Groups'), 'admin/og/og'))),
    '#required' => TRUE,
    '#disabled' => TRUE,
  );

  $form['gitolite_repository'] = array(
    '#type' => 'textfield',
    '#title' => t('Full URL of gitolite-admin repository'),
    '#default_value' => variable_get('gitolite_repository', ''),
    '#description' => t("The url of the gitolite-admin repository. For example
      'git@domain.to.my.repo:gitolite-admin.git'."),
    '#required' => TRUE,
  );

  $form['gitolite_main_config'] = array(
    '#type' => 'textarea',
    '#title' => t('Main configuration for gitolite.conf'),
    '#default_value' => variable_get('gitolite_main_config', ''),
    '#description' => t("This will be the main configutarion at the top of the gitolite.conf file."),
    '#required' => FALSE,
    '#wysiwyg'  => FALSE,
  );

  $form['gitolite_extra_config'] = array(
    '#type' => 'textarea',
    '#title' => t('Additional configuration for gitolite.conf'),
    '#default_value' => variable_get('gitolite_extra_config', ''),
    '#description' => t("This will be appended at the end of the generated gitolite.conf."),
    '#required' => FALSE,
    '#wysiwyg'  => FALSE,
  );

  $form['gitolite_main_config_help'] = array(
    '#value' => t("<b>Your main gitolite configuration should look at least as the " .
                  "following:</b> <br/>" .
                  "repo  gitolite-admin<br/>" .
                  "&nbsp;&nbsp;RW+ = name_of_your_username_at_keydir_directory<br/>" .
                  "repo  testing<br/>" .
                  "&nbsp;&nbsp;RW+ = @all<br/>"),
  );

  // Check if we have to force the user to select a content type to install "Git repositories" field
  if (!variable_get('gitolite_content_type_name', FALSE)) {
    $form['gitolite_content_type_name']['#disabled'] = FALSE;
    $form['gitolite_repository']['#disabled'] = TRUE;
    $form['gitolite_main_config']['#disabled'] = TRUE;
    $form['gitolite_extra_config']['#disabled'] = TRUE;
    $form['gitolite_repository']['#required'] = FALSE;
    $form['gitolite_main_config']['#required'] = FALSE;
    $form['gitolite_extra_config']['#required'] = FALSE;
    $form['#submit'][] = '_gitolite_admin_form_submit';
  }
  return system_settings_form($form);
}

/**
 * Custom submit handler for settings form.
 * Adds the Git repositories field to a content type
 */
function _gitolite_admin_form_submit($form_id, $form_values) {
  $content_type = $form_values['values']['gitolite_content_type_name'];
  gitolite_add_git_repositories_field($content_type);
  drupal_set_message(t('The "Git repositories" field was added to the selected content type.'));
}
