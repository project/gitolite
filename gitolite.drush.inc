<?php
/**
 * @file
 * Defines tasks to interact with gitolite administration.
 */

define('GITOLITE_REPOSITORY', sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'gitolite-admin' . DIRECTORY_SEPARATOR);

/**
 * Implementation of hook_drush_command().
 *
 * Defines gitolite's tasks such as load general config from gitolite.conf,
 * loading existing keys or updating gitolite-admin.git.
 *
 * @return
 *   An associative array describing your command(s).
 */
function gitolite_drush_command() {
  $items = array();

  $items['gitolite-load-config'] = array(
    'description' => "Loads existing configuration at gitolite.conf into admin/settings/gitolite.\n" .
                     "This is very important on first run to load the information of the gitolite-admin" .
                     "repository from gitolite.conf.",
    'arguments' => array(),
    'options' => array(),
    'examples' => array(
      'drush gitolite-load-config',
    ),
    'aliases' => array(),
    'callback' => 'gitolite_drush_load_config',
  );

  $items['gitolite-load-keys'] = array(
    'description' => "Loads existing public keys into sshkey table, looking for them in " .
                     "the keydir directory of 'gitolite-admin' repository.",
    'arguments' => array(),
    'options' => array(),
    'examples' => array(
      'drush gitolite-load-keys',
    ),
    'aliases' => array(),
    'callback' => 'gitolite_drush_load_keys',
  );

  $items['gitolite'] = array(
    'description' => "Clones gitolite-admin repository, updates keys, recreates gitolite-conf.git, " .
                     "commits and pushes changes.",
    'arguments' => array(),
    'options' => array(),
    'examples' => array(
      'drush gitolite',
    ),
    'aliases' => array(),
    'callback' => 'gitolite_drush_gitolite',
  );

  return $items;
}

/**
 * Callback for task gitolite-load-config
 *
 * Loads existing configuration at gitolite.conf into your module information.
 * This is very important on first run to load the information of the gitolite-admin
 * repository from gitolite.conf.
 */
function gitolite_drush_load_config() {
  _gitolite_load_gitolite_admin();
  $current_conf = file_get_contents(GITOLITE_REPOSITORY . 'conf/gitolite.conf');
  variable_set('gitolite_main_config', trim($current_conf));
  drush_print("\nThe information from gitolite.conf could be loaded successfully. Go to " .
              "admin/gitolite/settings to verify it.\n");
}

/**
 * Loads all public keys in keydir which match an email in the users table into sshkey table.
 */
function gitolite_drush_load_keys() {
  module_load_include('inc', 'sshkey', 'sshkey.pages');
  _gitolite_load_gitolite_admin();
  $iterator = new DirectoryIterator(GITOLITE_REPOSITORY . 'keydir');
  foreach ($iterator as $fileinfo) {
    if ($fileinfo->isFile() && preg_match('/\.pub$/', $fileinfo->getFilename())) {
      $key = file_get_contents($fileinfo->getPathname());
      // Match public key with user through email
      $user = db_fetch_object(db_query('SELECT *
                                        FROM {users} u
                                        WHERE u.mail = \'%s\'',
                                        preg_replace('/\.pub$/', '', $fileinfo->getFilename())));
      if ($user->uid) {
        // Check that the key does not exist already
        $exists = db_result(db_query('SELECT 1
                                      FROM {sshkey}
                                      WHERE entity_id = %d AND value = \'%s\'',
                                      $user->uid, $key));
        if (!$exists) {
          _gitolite_add_key($fileinfo->getFilename(), $key, $user->uid);
        }
      }
      else {
        _gitolite_add_key($fileinfo->getFilename(), $key, 1);
      }
    }
  }
}

/**
 * Callback for task 'gitolite'
 *
 * Updates gitolite configuration based on db info
 * Clones repository into temp folder, updates it and pushes changes
 */
function gitolite_drush_gitolite() {
  _gitolite_load_gitolite_admin();
  $main_config  = variable_get('gitolite_main_config', '');
  $extra_config = variable_get('gitolite_extra_config', '');

  if (!preg_match('/repo( )+gitolite-admin/', $main_config)) {
    throw new Exception('Group gitolite-admin has not been set at Main configuration field at adming/gitolite/settings. Enter it yourself or run gitolite-load-config to grab it from the existing gitolite.conf file');
  }

  $new_conf = $main_config . "\n\n";

  // Iterate groups with repositories
  $result_groups = db_query('SELECT DISTINCT n.nid, n.title
                             FROM {node} n
                             INNER JOIN {content_field_repository} cfr USING (nid)
                             WHERE n.type = \'group\'');
  while ($group = db_fetch_object($result_groups)) {
    // Iterate users within a group and add those with a public key
    $result_users = db_query('SELECT u.mail, u.uid
                                FROM {users} u
                                INNER JOIN {og_uid} og USING(uid)
                                WHERE og.nid = %d AND u.status = 1
                                  AND u.uid <> 1', $group->nid);
    $users = array();
    while ($user = db_fetch_object($result_users)) {
      if (db_result(db_query('SELECT 1
                              FROM {sshkey}
                              WHERE entity_type = \'user\'
                                AND entity_id = %d', $user->uid))) {
        $users[] = $user->mail;
      }
    }
    $new_conf .= '# ' . $group->title . "\n";

    // Iterate group repositories
    $result_repos = db_query('SELECT field_repository_value AS name
                              FROM {content_field_repository}
                              WHERE NID = %d', $group->nid);
    $repos = array();
    while ($group_repo = db_fetch_object($result_repos)) {
      $repos[] = $group_repo->name;
    }
    $new_conf .= 'repo ' . implode(' ', $repos) . "\n";
    $new_conf .= '  RW+ = ' . implode(' ', $users) . "\n";
    $new_conf .= "\n";
  }

  $new_conf .= "\n\n" . trim($extra_config);

  chdir(GITOLITE_REPOSITORY);
  $fh = fopen('conf/gitolite.conf', 'w');
  fwrite($fh, $new_conf);
  fclose($fh);

  // Add keys to keydir (ignoring if they already exist or not)
  $result_keys = db_query('SELECT u.mail, s.*
                           FROM {users} u
                           INNER JOIN {sshkey} s ON u.uid = s.entity_id');
  while ($key = db_fetch_object($result_keys)) {
    // Admin user keys are named by key title and not by email
    // This is because the admin user will probably have other keys
    // for non drupal users such as development server keys.
    $key_title = $key->entity_id == 1?$key->title:$key->mail;
    $full_key_title = preg_match('/\.pub$/', $key->title)?$key->title:$key->title . '.pub';
    $fh = fopen('keydir/' . $full_key_title, 'w');
    fwrite($fh, $key->value);
    fclose($fh);
  }

  $result = exec('git add .', $response, $return_code);
  $result = exec('git commit -m "Updated repository"', $response, $return_code);
  $result = exec('git push', $response, $return_code);
}

/**
 * Private function to load 'gitolite-admin' repository
 *
 * @throws Exception
 */
function _gitolite_load_gitolite_admin() {
  $repository = variable_get('gitolite_repository', '');
  if (!$repository) {
    throw new Exception('The gitolite repository URL has not been set');
  }
  else {
    if (is_dir(GITOLITE_REPOSITORY)) {
      $result = exec('rm -rf ' . GITOLITE_REPOSITORY);
    }
    drush_print('Cloning gitolite-admin to temporary dir ' . GITOLITE_REPOSITORY);
    $command = 'git clone ' . variable_get('gitolite_repository', '') . ' ' . GITOLITE_REPOSITORY;
    $result = exec($command, $response, $return_code);
  }
  if (!file_exists(GITOLITE_REPOSITORY . 'conf/gitolite.conf')) {
    throw new Exception('The gitolite repository could not be cloned');
  }
}

/**
 * Private function to add an ssh key for a user
 *
 * @param $title
 *   The key title, which is used to store the key name
 *   and then referenced in each repository of gitolite.conf
 *
 * @param $key
 *   The string of characters which form the ssh public key.
 *
 * @param $uid
 *   The user's id
 */
function _gitolite_add_key($title, $key, $uid) {
  $form_state = array();
  $form_state['values']['title'] = $title;
  $form_state['values']['value'] = $key;
  drupal_execute('sshkey_edit_form', $form_state, 'user', $uid);
  $user = user_load($uid);
  if (form_get_errors()) {
    drush_print('The public key ' . $title . ' could not be saved for user ' . $user->mail . ' in database
                 because of the following error(s): ' . print_r(form_get_errors(), TRUE));
  }
  else {
    drush_print('Successfully added public key with title ' . $title . ' for user with email ' . $user->mail);
  }
}

/**
 * Private function to create safe ids
 *
 * @credits http://api.lullabot.com/zen_id_safe/5
 */
function _gitolite_zen_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'id' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id' . $string;
  }
  return $string;
}
