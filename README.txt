CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Related modules
* Troubleshooting

INTRODUCTION
============
Gitolite module automates user access to a set of git repositories. Each group 
defines a list of git repositories which are accessible by all the users in it.

This module adds the following interactions:
* admin/settings/gitolite: allows you to set the url to your gitolite-admin 
  repository (such as gitolite@localhost:gitolite-admin.git).
* When editing a group, a section called "Git repositories" lets you define them.
  These repositories will be created the next time the drush task "gitolite" runs
  and all the users in the group will have read and write access over them.
* When a user edits his account, a section called "SSH Keys" let's her add public
  keys (thanks to sshkey module).
* There are three drush tasks involved:
  * 'gitolite' is the main task that will update your gitolite configuration
    and add new public keys from your users.
  * 'gitolite-load-config' takes your current gitolite.conf and saves it at
    admin/settings/gitolite > Main configuration.
  * 'gitolite-load-keys' looks for public keys at gitolite's keydir directory and
    attempts to match them with your users email, adding them if so.

INSTALLATION
============
1. You must have previously installed gitolite and had verified that you have rights
   to clone gitolite-admin.git.
   For more instructions on how to install gitolite, refer to the following page:
     * https://github.com/sitaramc/gitolite
2. Make sure that the modules Content (CCK), Text (CCK), Organic Groups and 
   Sshkey are enabled. Drush is also needed.
2. The Content Type where Gitolite repositories will be managed must be called 
   "group". Otherwise the module will fail to add the field that manages repositories
   in a group (this is configured by default if you are using Open Atrium).
3. Enable the module, clear the cache and go to admin/settings/gitolite. In the first 
   field, set your gitolite-admin url (ie. gitolite@localhost:gitolite-admin.git). In 
   order to test it, run the following drush task:
     drush gitolite-load-config
   If the task completed with no errors, it will load the contents of your gitolite.conf
   file into the field "Main configuration for gitolite.conf" at admin/settings/gitolite.
4. If you want users to add themselves their public keys, grant the following permissions 
   to role "Authenticated user": "manage own SSH public keys" and "view own SSH public keys".
5. If there are public keys at keydir/ that you want to load into Drupal, run the 
   following task:
     drush gitolite-load-keys
6. Once you have created some groups, added users to them and set repositories at each group
   settings, update gitolite's information with the following task:
     drush gitolite
   Which does:
     1. Clones gitolite-admin to a temporary directory.
     2. Recreates conf/gitolite.conf with the "Main configuration" setting, the info from each
        group, and the "Extra configuration" setting.
     3. Adds new keys and update existing ones.
     4. Pushes back to update the gitolite configuration.
   If you want to verify the result, clone the gitolite-admin repository and check the files and
   the log.
7. In order to automate the process of updating your gitolite-conf repository, you need 
   to set a crontab that runs it periodically. The frequency of it depends on you, but the
   recommended is to make it run every minute, such as the following example:
     crontab -e # opens your crontab console. Then type:
     */1 * * * * /path/to/php drush gitolite --root=/root/directory/of/your/site
   For example, in my local box I have:
     */5 * * * * /usr/bin/php /home/juampy/drupal/sites/all/modules/drush/drush.php \ 
                                   gitolite --root=/home/juampy/Projects/drupal/site

RELATED MODULES
===============
http://drupal.org/project/sshkey
http://drupal.org/project/og
http://drupal.org/project/cck
http://drupal.org/project/drush
